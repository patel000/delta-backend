﻿using AutoMapper;
using Delta.Interface;
using Delta.Model;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Delta.Controllers
{
    [Route("user")]
    public class UserController : Controller
    {
        
        private readonly IUser user;
        public UserController(IUser user)
        {
            this.user = user;
            
        }

        /// <summary>
        /// Deletes a specific TodoItem.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost("authenticate")]
        [AllowAnonymous]
        public async Task<IActionResult> authenticateUser(UserDto userDto)
        {
            var token = await user.Authentcation(userDto.UserName, userDto.Password);

            if (token == null)
            {
                return Unauthorized();
            }

            return Ok(token);
        }

        [HttpPost("authenticates")]
        [AllowAnonymous]
        public async Task<IActionResult> authenticateUser(TokenResponse tokenResponse)
        {
            var token = await user.GetNewAccessToken(tokenResponse.token, tokenResponse.refreshtoken);
            return Ok(token);
        }

         [HttpGet("getUser/{id}")]
        public async Task<IActionResult> getUserByAdmin(int adminId)
        {

            var users = await user.GetUserByAdmin(adminId);
            return Ok(users);
        }
        [HttpPost("register")]
        public async Task<IActionResult> RegisterUser(User userObj)
        {

            await user.RegisterUser(userObj);
            return Ok("Added Successfully");
        }
    }
}
