﻿using Delta.Interface;
using Delta.Model;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Delta.Repo
{
    public class UserService : IUser
    {
        private readonly DeltaDbContext _db;
        private readonly IConfiguration configuration;

        public UserService(DeltaDbContext db, IConfiguration configuration)
        {
            this._db = db;
            this.configuration = configuration;
        }



        public async Task<TokenResponse> Authentcation(string username, string passwrod)
        {

       
             var user =  _db.users.Where(u => u.Code == username && u.Password == passwrod).FirstOrDefault();
            if (user == null)
            {
                return null;
            }
            var tokenvalue = GetToken(user.Id);
            var refreshToken = GetRefreshToken();
            RefreshToken refresh = new RefreshToken();
            refresh.refreshToken = refreshToken;
            refresh.UserId = user.Id;
            refresh.ExpireDateTime = DateTime.UtcNow.AddMonths(6);
            _db.refreshTokens.Add(refresh);
            _db.SaveChanges();
            TokenResponse tokenResponse = new TokenResponse { token = tokenvalue, refreshtoken = refreshToken };
            return tokenResponse;
        }

        private string GetToken(int id)
        {
            var tokenHandler = new System.IdentityModel.Tokens.Jwt.JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(configuration.GetSection("AppSettings:value").Value);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, id.ToString()),
                }),
                Expires = DateTime.UtcNow.AddSeconds(60),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);

            var tokenvalue = tokenHandler.WriteToken(token);
            return tokenvalue;
        }

        private string GetRefreshToken()
        {
            var randomNumber = new byte[32];
            using (var rng = RandomNumberGenerator.Create())
            {
                rng.GetBytes(randomNumber);
                return Convert.ToBase64String(randomNumber);
            }
        }

        private ClaimsPrincipal GetPrincipalFromExpiredToken(string token)
        {
            var tokenValidationParameters = new TokenValidationParameters
            {
                ValidateAudience = false, //you might want to validate the audience and issuer depending on your use case
                ValidateIssuer = false,
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration.GetSection("AppSettings:value").Value)),
                ValidateLifetime = false //here we are saying that we don't care about the token's expiration date
            };

            var tokenHandler = new JwtSecurityTokenHandler();
            SecurityToken securityToken;
            var principal = tokenHandler.ValidateToken(token, tokenValidationParameters, out securityToken);
            var jwtSecurityToken = securityToken as JwtSecurityToken;
            if (jwtSecurityToken == null || !jwtSecurityToken.Header.Alg.Equals(SecurityAlgorithms.HmacSha256, StringComparison.InvariantCultureIgnoreCase))
                throw new SecurityTokenException("Invalid token");

            return principal;
        }
        public async Task<TokenResponse> GetNewAccessToken(string token, string refreshtoken)
        {
            var principal = GetPrincipalFromExpiredToken(token);
            int username = int.Parse(principal.Identity.Name);
            var refData = await _db.refreshTokens.Where(r => r.UserId == username).SingleOrDefaultAsync();
            if (refData.refreshToken != refreshtoken)
                throw new SecurityTokenException("invalid refresh token");

            var newjwttoken = GetToken(username);
            var newrefreshtoken = GetRefreshToken();

            refData.refreshToken = newrefreshtoken;

            _db.SaveChanges();


            return new TokenResponse
            {
                token = newjwttoken,
                refreshtoken = newrefreshtoken
            };
        }

        public async Task<IList<User>> GetUserByAdmin(int id)
        {
            var users = await _db.users.Where(u => u.AdminId == id).ToListAsync();
            return users;
        }

        public async Task RegisterUser(User user)
        {
            await _db.users.AddAsync(user);
            _db.SaveChanges();
        }
    }
}
