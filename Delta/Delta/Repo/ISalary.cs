﻿using Delta.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Delta.Repo
{
    public interface ISalary
    {
        Task<long> GetSalaryOfUser(int userId);
        Task<IList<Salary>> GetSalaryOfAdmin(int adminId);
    }
}
