﻿using Delta.Interface;
using Delta.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Delta.Repo
{
    public class SalaryService : ISalary
    {
        private readonly DeltaDbContext _db;
        private readonly IUser user;
        public SalaryService(DeltaDbContext db,IUser user)
        {
            this._db = db;
            this.user = user;
        }
        public async Task<IList<Salary>> GetSalaryOfAdmin(int adminId)
        {
            var users = await user.GetUserByAdmin(adminId);
            IList<Salary> salaries = null;
            foreach(User user in users)
            {
                var salary =  _db.salaries.First(s => s.UserId == user.Id);
                salaries.Add(salary);
            }
            return salaries;
        }

        public async Task<long> GetSalaryOfUser(int userId)
        {
            var salaryAmount =  _db.salaries.First(s => s.UserId == userId).Amount;
            return salaryAmount;
        }
    }
}
