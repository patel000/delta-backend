﻿using Delta.Interface;
using Delta.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Delta.Repo
{
    public class WithdrawalService : IWithdrawal
    {
        private readonly DeltaDbContext _db;
        private readonly IUser user;
        public WithdrawalService(DeltaDbContext db, IUser user)
        {
            this._db = db;
            this.user = user;
        }
        public async Task<IList<Withdrawal>> GetWithdrawalOfAdmin(int adminId)
        {
            var users = await user.GetUserByAdmin(adminId);
            IList<Withdrawal> withdrawals = null;
            foreach (User user in users)
            {
                var salary = _db.withdrawals.First(s => s.UserId == user.Id);
                withdrawals.Add(salary);
            }
            return withdrawals;
        }

        public async Task<long> GetWithdrawalOfUser(int userId)
        {
            var salaryAmount = _db.withdrawals.First(s => s.UserId == userId).Amount;
            return salaryAmount;
        }
    }
}
