﻿using Delta.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Delta.Repo
{
    public interface IWithdrawal
    {
        Task<long> GetWithdrawalOfUser(int userId);
        Task<IList<Withdrawal>> GetWithdrawalOfAdmin(int adminId);
    }
}
