﻿using Delta.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Delta.Interface
{
    public interface IUser
    {
        Task RegisterUser(User user);
        Task<TokenResponse> Authentcation(string username, string passwrod);
        Task<TokenResponse> GetNewAccessToken(string token, string refreshtoken);

        Task<IList<User>> GetUserByAdmin(int adminId);
    }
}
