﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Delta.Model
{
    public class RefreshToken
    {
        [Key]
        public int TokenId { get; set; }
        [Required]
        public string refreshToken { get; set; }
        [Required]
        public DateTime ExpireDateTime { get; set; }

        [ForeignKey("userid")]
        public int UserId { get; set; }
        public User user { get; set; }
    }
}
