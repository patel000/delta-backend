﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Delta.Model
{
    public class DeltaDbContext : DbContext
    {
      
      
        public DbSet<User> users { get; set; }
        public DbSet<RefreshToken> refreshTokens { get; set; }
        public DbSet<Salary> salaries { get; set; }
        public DbSet<Withdrawal> withdrawals { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite(@"Data Source=E:\Project\Delta\Delta-Back\delta-backend\Delta\Delta\Delta.db");
        }
    }
}
