﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Delta.Model
{
    [Table("salaryMapping")]
    public class SalaryMapping
    {
        [Key]
        public int Id { get; set; }
        [ForeignKey("user")]
        public int UserId { get; set; }
        public User user { get; set; }
        public string Name { get; set; }

    }
}
