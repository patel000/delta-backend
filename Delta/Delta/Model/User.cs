﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Delta.Model
{
    [Table("User")]
    public class User
    {
        [Key]
        public int Id { get; set; }
        public int AdminId { get; set; }
        public string Name { get; set; }
        public string Password { get; set; }
        public string Pancard { get; set; }
        public string Code { get; set; }

    }
}