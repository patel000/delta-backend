﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Delta.Model
{
    [Table("Withdrawal")]
    public class Withdrawal
    {
        [Key]
        public string Id { get; set; }
        [ForeignKey("user")]
        public int UserId { get; set; }
        public User user { get; set; }
        public int Month { get; set; }
        public long Amount { get; set; }
    }
}
